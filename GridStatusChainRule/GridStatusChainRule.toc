## Interface: 20003
## Title: GridStatusChainRule |cff7fff7f -Ace2-|r
## Notes: Indicates optimal targets for chain heal. Based on GridStatusLifebloom by Coedan@Neptulon-EU
## Author: Tyde@Spinebreaker
## Version: 1.0
## Dependencies: Grid
## OptionalDeps: Ace2, Babble-2.2, RosterLib, SpecialEventsEmbed
## X-GridStatusModule: GridStatusChainRule

## Ace Libraries (not using anything beyond what Grid already requires)
##libs\AceDebug-2.0\AceDebug-2.0.lua
##libs\AceLocale-2.2\AceLocale-2.2.lua

##libs\Roster-2.1\Roster-2.1.lua
##libs\SpecialEvents-Aura-2.0\SpecialEvents-Aura-2.0.lua
##libs\Babble-Spell-2.2\Babble-Spell-2.2.lua

GridStatusChainRule.lua