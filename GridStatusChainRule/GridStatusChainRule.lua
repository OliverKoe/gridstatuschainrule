--Copyright 2008 Chip Hilseberg (Tyde-Spinebreaker)
--Inspiration from GridStatusLifebloom (Coedan@Neptulon-EU), LibCommHeal

--This file is part of GridStatusChainRule.
--
--    GridStatusChainRule is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    GridStatusChainRule is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--   along with GridStatusChainRule.  If not, see <http://www.gnu.org/licenses/>.

--{{{ Libraries

--local Aura = AceLibrary("SpecialEvents-Aura-2.0")
local L = AceLibrary("AceLocale-2.2"):new("GridStatusChainRule")
local BS = AceLibrary("Babble-Spell-2.2")
local RL = AceLibrary("Roster-2.1")

--}}}
--
--{{{ Libraries

L:RegisterTranslations("enUS", function()
	return {
		["Chain Rule"] = true,
		["Targetting"] = true,
		["Density"] = true,
		["Enable"] = true,
		["Enable Heal Targetting"] = true,
                ["Enable Density Measurements"] = true,

		["Color CH1"] = true,
		["Color CH4"] = true,
		["Color CH5"] = true,

		["Color when player is an efficient target for CH5 or lower"] = true,
		["Color when player is an efficient target for CH4 or lower"] = true,
		["Color when player is an efficient target for CH1"] = true,
        }
end)


GridStatusChainRule = GridStatus:NewModule("GridStatusChainRule")
GridStatusChainRule.menuName = L["Chain Rule"]
ChainHealFirstJump = {}
JumpTable = {}

GridStatusChainRule.defaultDB = {
	debug = false,
	alert_Targetting = {
		text = L["Targetting"],
		enable = true,
		color = { r = 0, g = 1, b = 0, a = 1 },
		priority = 70,
		range = false,
		color2 = {r = 1, g = 1, b = 0, a = 1},
		color3 = { r = 1, g = 0, b = 0, a = 1 },
	},
	alert_Density = {
                text = L["Density"],
                enable = true,
                color = { r = 0, g = 1, b = 0, a = 1 },
                priority = 70,
                range = false,
        },
}


local alert_TargettingOptions = {
        ["enable"] = {
                 type = "toggle",
		name = L["Enable"],
		desc = L["Enable Heal Targetting"],
		order = 112,
		get = function ()
			      return GridStatusChainRule.db.profile.alert_Targetting.enable
		      end,
		set = function (v)
                              GridStatusChainRule:TargettingToggled(v)
			      GridStatusChainRule.db.profile.alert_Targetting.enable = v
		      end,
        },
        ["color"] = {
                type = "color",
		name = L["Color CH1"],
		desc = L["Color when player is an efficient target for CH1"],
		hasAlpha = true,
		get = function ()
			local color = GridStatusChainRule.db.profile.alert_Targetting.color
			return color.r, color.g, color.b, color.a
		end,
		set = function (r, g, b, a)
			local color = GridStatusChainRule.db.profile.alert_Targetting.color
			color.r = r
			color.g = g
			color.b = b
			color.a = a or 1
		end,
	},
	["color2"] = {
		type = "color",
		name = L["Color CH4"],
		desc = L["Color when player is an efficient target for CH4 or lower"],
		hasAlpha = true,
		get = function ()
			local color = GridStatusChainRule.db.profile.alert_Targetting.color2
			return color.r, color.g, color.b, color.a
		end,
		set = function (r, g, b, a)
			local color = GridStatusChainRule.db.profile.alert_Targetting.color2
			color.r = r
			color.g = g
			color.b = b
			color.a = a or 1
		end,
	},
       ["color3"] = {
		type = "color",
		name = L["Color CH5"],
		desc = L["Color when player is an efficient target for CH5 or lower"],
		hasAlpha = true,
		get = function ()
			local color = GridStatusChainRule.db.profile.alert_Targetting.color3
			return color.r, color.g, color.b, color.a
		end,
		set = function (r, g, b, a)
			local color = GridStatusChainRule.db.profile.alert_Targetting.color3
			color.r = r
			color.g = g
			color.b = b
			color.a = a or 1
		end,
	},

}

local alert_DensityOptions = {
        ["enable"] = {
                type = "toggle",
		name = L["Enable"],
		desc = L["Enable Density Measurements"],
		order = 112,
		get = function ()
			      return GridStatusChainRule.db.profile.alert_Density.enable
		      end,
		set = function (v)
                              GridStatusChainRule:DensityToggled(v)
			      GridStatusChainRule.db.profile.alert_Density.enable = v
		      end,
        },

}


function GridStatusChainRule:OnInitialize()
	self.super.OnInitialize(self)
	self:RegisterStatus('alert_Targetting', L["Targetting"], alert_TargettingOptions)
	self:RegisterStatus('alert_Density', L["Density"], alert_DensityOptions)
	self:RecalculateHealing()
end

function GridStatusChainRule:OnEnable()

        self:RegisterEvent("PLAYER_ENTERING_WORLD", "UpdateAllUnits")
	self:RegisterEvent("Grid_UnitJoined")
	self:RegisterEvent("Grid_UnitDeath")
	self:RegisterEvent("Grid_UnitChanged")
	self:RegisterEvent("CHAT_MSG_ADDON")
	self:RegisterEvent("Grid_LeftParty")
	
	if(GridStatusChainRule.db.profile.alert_Targetting.enable) then
	    self:RegisterEvent("UNIT_AURA")
            self:RegisterEvent("UNIT_HEALTH", "UpdateEffectedUnits")
            self:RegisterEvent("UNIT_MAXHEALTH", "UpdateEffectedUnits")
	end

end

function GridStatusChainRule:DensityToggled(enabled)

         if(not enabled) then
              for name in self.core:CachedStatusIterator("alert_Density") do
    		   self.core:SendStatusLost(name, "alert_Density")
              end
         end

end

function GridStatusChainRule:Grid_LeftParty()

          for name in self.core:CachedStatusIterator("alert_Density") do
    		   self.core:SendStatusLost(name, "alert_Density")
          end
          
          for name in self.core:CachedStatusIterator("alert_Targetting") do
    		   self.core:SendStatusLost(name, "alert_Targetting")
          end
end

function GridStatusChainRule:UpdateDensity(unit)

           local name = UnitName(unit)

           if(JumpTable[name] == nil or JumpTable[name]["players"] == nil) then
               return;
           end

           local others = #JumpTable[name]["players"]
           
           if(others <= 0) then
               theColor = { r = 1, g = 0, b = 0, a = 0}
           elseif(others == 1) then
               theColor = { r = 1, g = 1, b = 0, a = 1}
           else
               theColor = { r = 0, g = 0.5, b = 0, a = 1}
           end
           
           -- logic that figures out the real jumping targets, but requires most people to have the mod for it to be useful
--             if(others == 0) then
--                 theColor = { r = 1, g = 0, b = 0, a = 1}
--             else
--                 local thirdJump = false
--                 local me = UnitName("player")
--                 for i,v in ipairs(JumpTable[name]["players"]) do
--                     if((JumpTable[v] ~= nil) and (JumpTable[v]["players"] ~= nil) and (v ~= me) and #JumpTable[v]["players"] > 0) then
--                         thirdJump = true
--                         break
--                     end
--                 end
--
--                if (not thirdJump) then
--                      theColor = { r = 1, g = 1, b = 0, a = 1}
--                 else
--                      theColor = { r = 0, g = 1, b = 0, a = 1}
--                 end
--             end


           local settings = self.db.profile.alert_Density

           self.core:SendStatusGained(name, "alert_Density",
			settings.priority,
			(settings.range and 40),
			theColor,
			tostring(others),
			others,
			25 )
end

function GridStatusChainRule:TargettingToggled(enabled)
         if(enabled) then
               self:RegisterEvent("UNIT_AURA")
               self:RegisterEvent("UNIT_HEALTH", "UpdateEffectedUnits")
	       self:RegisterEvent("UNIT_MAXHEALTH", "UpdateEffectedUnits")
         else
               self:UnregisterEvent("UNIT_AURA")
               self:UnregisterEvent("UNIT_HEALTH")
               self:UnregisterEvent("UNIT_MAXHEALTH")
               local status, moduleName, desc

               for name in self.core:CachedStatusIterator("alert_Targetting") do
    		   self.core:SendStatusLost(name, "alert_Targetting")
               end
        end

end

function GridStatusChainRule:UpdateEffectedUnits(unitid)

         self:UpdateUnit(unitid)
         
         local name = UnitName(unitid)

         if( JumpTable[name] ~= nil and JumpTable[name]["players"] ~= nil and (time() - JumpTable[name]["time"]) < 10) then
            for i,v in ipairs(JumpTable[name]["players"]) do
               self:UpdateUnit(v)
            end
         end

end

function GridStatusChainRule:CHAT_MSG_ADDON(prefix, msg, type, sender)

       if( prefix == "GSCR") then
           if( string.sub(msg,1,4) == "CONT" and JumpTable[sender] ~= nil and JumpTable[sender]["players"] ~= nil ) then
               msg = string.sub(msg,6)
           else
               JumpTable[sender] = {}
               JumpTable[sender]["players"] = {}
           end

           --DEFAULT_CHAT_FRAME:AddMessage(sender .. " " .. msg)

           JumpTable[sender]["time"] = time()
           
           for player in string.gmatch (msg, "[^;]+") do
               table.insert(JumpTable[sender]["players"], player)
           end

           if(GridStatusChainRule.db.profile.alert_Targetting.enable) then
               self:UpdateUnit(sender)
           end
           
           if(GridStatusChainRule.db.profile.alert_Density.enable) then
               self:UpdateDensity(sender)
           end
       end

end

function GridStatusChainRule:UNIT_AURA(unitid)
         if(GridStatusChainRule.db.profile.alert_Targetting.enable and (UnitName(unitid) == UnitName("player"))) then
            self:RecalculateHealing()
         end
end

--Code Borrowed From: LibHealComm-3.0
local relicSlotNumber = GetInventorySlotInfo("RangedSlot");
local function getEquippedRelicID()
    local itemLink = GetInventoryItemLink('player', relicSlotNumber);
    if (itemLink) then
        return tonumber(itemLink:match("(%d+):"));
    end
end

--Some Code Borrowed From: LibHealComm-3.0
function GridStatusChainRule:RecalculateHealing()
         
        -- Get +healing bonus
        local bonus = GetSpellBonusHealing();

        -- Purification Talent (increases healing by 2% per rank)
        local _, _, _, _, talentPurification = GetTalentInfo(3,15);
        talentPurification = 2 * talentPurification / 100 + 1;

        -- Totem of Healing Rains
        local totemBonus = (getEquippedRelicID() == 28523) and 87 or 0;

        -- Improved Chain Heal Talent (increases healing by 10% per rank)
        local _, _, _, _, talentImprovedChainHeal = GetTalentInfo(3,19);
        talentImprovedChainHeal = 10 * talentImprovedChainHeal / 100 + 1;

        ChainHealFirstJump[1] = talentImprovedChainHeal * talentPurification * (344 + totemBonus + ((2.5 / 3.5) * bonus) * 0.729);
        ChainHealFirstJump[2] = talentImprovedChainHeal * talentPurification * (648 + totemBonus + ((2.5 / 3.5) * bonus));
        ChainHealFirstJump[3] = talentImprovedChainHeal * talentPurification * (884 + totemBonus + ((2.5 / 3.5) * bonus));
        
end

function GridStatusChainRule:Grid_UnitChanged(name, unitid)
           
        if(GridStatusChainRule.db.profile.alert_Targetting.enable) then
            self:UpdateEffectedUnits(unitid)
        end
        if(GridStatusChainRule.db.profile.alert_Density.enable) then
            self:UpdateDensity(unitid)
        end
end

--Broken (doesn't reset the event handlers)
function GridStatusChainRule:Reset()
	self.super.Reset(self)
	self:UpdateAllUnits()
end

function GridStatusChainRule:Grid_UnitJoined(name, unitid)
	if unitid then
            if GridStatusChainRule.db.profile.alert_Targetting.enable then
        	self:UpdateEffectedUnits(unitid)
	    end
        end
end

--Might fail some of the time
function GridStatusChainRule:Grid_UnitDeath(unitname)
	local status, moduleName, desc
	for status, moduleName, desc in self.core:RegisteredStatusIterator() do
		if moduleName == self.name then
			self.core:SendStatusLost(unitname, status)
		end
	end
end

function GridStatusChainRule:UpdateAllUnits()
	
        for u in RL:IterateRoster() do
		self:UpdateUnit(u.unitid)
	end
end

function GridStatusChainRule:UpdateUnit(unitid)

    local name = UnitName(unitid)
    
    if not name then return end
    if UnitIsDeadOrGhost(unitid) then return end
    if string.find(unitid, "pet") then return end

    local HP = UnitHealth(unitid)
    local maxHP = UnitHealthMax(unitid)
    local settings = self.db.profile.alert_Targetting

    local maxEfficientRank = 0

    local i
    for i = 1,3 do
        local totalHealable = ChainHealFirstJump[i]*1.5
        local myHealable = min(maxHP - HP, ChainHealFirstJump[i]);

        local theirHealable = 0
        local halfChain = ChainHealFirstJump[i]*0.5
        
        if( JumpTable[name] ~= nil and JumpTable[name]["players"] ~= nil and (time() - JumpTable[name]["time"]) < 10) then
            for i,v in ipairs(JumpTable[name]["players"]) do
                local def = UnitHealthMax(v) - UnitHealth(v)
                if(def > theirHealable) then
                       theirHealable = min(def, halfChain)
                end
            end
        else
            theirHealable = 0
        end

        if(theirHealable == 0) then
            maxEfficientRank = 0
            break;
        end

        local theValue = math.ceil(((myHealable + theirHealable) / totalHealable)*100)
        
        if theValue < 80 then
           break
        else
            maxEfficientRank = maxEfficientRank + 1
        end
    end

    local theColor = settings.color
    local priority = settings.priority
    local message = ""

    if(maxEfficientRank == 3) then
        theColor = settings.color3
        message = "CH 5"
    elseif(maxEfficientRank == 2) then
        theColor = {r = 1, g = 1, b = 0, a = 1}
        message = "CH 4"
    elseif(maxEfficientRank == 1) then
        theColor = settings.color
        message = "CH 1"
    elseif(maxEfficientRank == 0) then
        priority = 0
    end

    self.core:SendStatusGained(name, "alert_Targetting",
			priority,
			(settings.range and 40),
			theColor,
			message,
			maxEfficientRank,
			3 )



end