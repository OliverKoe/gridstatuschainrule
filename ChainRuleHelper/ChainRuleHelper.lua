--Copyright 2008 Chip Hilseberg (Tyde-Spinebreaker)

--This file is part of GridStatusChainRule.
--
--    GridStatusChainRule is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    GridStatusChainRule is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--   along with GridStatusChainRule.  If not, see <http://www.gnu.org/licenses/>.

inRaid = false
timerRunning = false
timerValue = 0

function ChainRuleHelperFrame_OnLoad()

   if(GetNumRaidMembers() > 0) then
       inRaid = true
       EnableUpdates()
   else
       inRaid = false
   end

   this:RegisterEvent("RAID_ROSTER_UPDATE")
   
   DEFAULT_CHAT_FRAME:AddMessage("Chain Rule Helper Loaded")

end

function EnableUpdates()

    DEFAULT_CHAT_FRAME:AddMessage("Sending Updates")
    
    timerRunning = true
    timerValue = 0

end

function DisableUpdates()

    DEFAULT_CHAT_FRAME:AddMessage("Stopping Updates")
    
    timerRunning = false
    
end

function ChainRuleHelperFrame_OnUpdate()

      if timerRunning then
        timerValue = timerValue + arg1
        if timerValue > 1 then
           timerValue = 0

           local msg = ""
           local i
           for i=0, GetNumRaidMembers() do
               name = GetRaidRosterInfo(i)
               if( (not UnitIsDeadOrGhost("raid"..i)) and CheckInteractDistance("raid" .. i, 2) ~= nil) then
                   if(name ~= UnitName('player')) then
                        if(string.len(name) + 1 + string.len(msg) < 250) then
                            msg = msg .. name .. ";"
                        else
                            --Send
                            SendAddonMessage("GSCR", msg, "RAID")
                            --SendAddonMessage("GSCR", msg, "WHISPER", "Tyde")
                            --Make new msg
                            msg = "CONT "
                        end
                   end
               end
           end
           --Send Msg
           SendAddonMessage("GSCR", msg, "RAID")
           --SendAddonMessage("GSCR", msg, "WHISPER", "Tyde")
        end
     end
end

function ChainRuleHelperFrame_OnEvent()

   if (event == "RAID_ROSTER_UPDATE") then
          if(GetNumRaidMembers() > 0 and inRaid == false) then
              inRaid = true
              EnableUpdates()
          elseif (GetNumRaidMembers() == 0 and inRaid == true) then
              inRaid = false
              DisableUpdates()
          end

   end
   
end